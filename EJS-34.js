const clientes = [
  {
    id: 1,
    nombre: 'Leonor'
  },
  {
    id: 2,
    nombre: 'Jacinto'
  },
  {
    id: 3,
    nombre: 'Waldo'
  }
]

const pagos = [
  {
    id: 1,
    pago: 1000,
    moneda: 'Bs'
  },
  {
    id: 2,
    pago: 1800,
    moneda: 'Bs'
  }
]

const id = 1;

// Callback

const getTienda = (id, callback)
  const cliente = clientes.find(clientes =>  clientes.id === id);
  setTimeout(() => {
    callback(cliente)
  });

  getTienda(2, (clientes) => {
    console.log(clientes);
  })

const getTienda2 = (id, callback)
  const pago = pagos.find(pagos => pagos.id === id);
  setTimeout(() => {
    callback(pago)
  });

  getTienda2(2, (pagos) => {
    console.log(pagos);
  })

// Promesa

// const getClientes = (id) => {
//   return new Promise (( resolve, reject ) => {
//     const cliente = clientes.find(c => c.id === id);
//     if (cliente) {
//       resolve(cliente);
//     } else {
//       reject(`No existe el cliente con el id ${id}`);
//     }
//   });
// };

// const getPago = (id) => {
//   return new Promise(( resolve, reject ) => {
//     const pago = pagos.find(p => p.id === id);
//     if (pago) {
//       resolve(pago);
//     } else {
//       reject(`No se realizo el pago con el id ${id}`);
//     }
//   });
// };

// getClientes(id)
//   .then(cliente => console.log(cliente))
//   .catch(error => {
//     console.log(error);
//   });

// getPago(id)
//   .then(pago => console.log(pago))
//   .catch(error => {
//     console.log(error);
//   });

// Promesa en cadena

// let nombre;

// getClientes(id)
//   .then (cliente => {
//     console.log(cliente);
//     nombre = cliente.nombre;
//     return getClientes(id)
//   })
//   .then (pago => {
//     console.log('El cliente', nombre,'hizo un pago de:', pago['pago']);
//   })
//   .catch (error => {
//     console.log(error);
//   });




// Asycn/Await

// const getInfoUsuario = async (id) => {
//   try {
//     const cliente = await getClientes(id);
//     const pago = await getPago(id);
//     return `El pago del cliente: ${cliente.nombre} es de ${pago['pago']}`;
//   }
//   catch(ex){
//     throw ex;
//   }
// }

// getInfoUsuario(id)
//   .then(msg => console.log(msg))
//   .catch(err => console.log(err));