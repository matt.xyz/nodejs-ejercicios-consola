const { generarArchivo } = require('../opAritmeticas/multiplicar');
const { generarArchivo1 } = require('../opAritmeticas/dividir');
const { generarArchivo2 } = require('../opAritmeticas/resta');
const { generarArchivo3 } = require('../opAritmeticas/suma');

//const base = 5;

//console.log(process.argv);
const [, , arg3 = 'base=1'] = process.argv;
const [, base] = arg3.split('=');
// console.log(base);

//Multiplicar
generarArchivo(base)
  .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
  .catch(err => console.log(err));

//Dividir
generarArchivo1(base)
  .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
  .catch(err => console.log(err));

//Resta
generarArchivo2(base)
  .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
  .catch(err => console.log(err));

//Suma
generarArchivo3(base)
  .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
  .catch(err => console.log(err));