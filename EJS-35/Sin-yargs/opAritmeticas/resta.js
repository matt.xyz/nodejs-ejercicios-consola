//Estoy importando el paquete file system (fs)
const fs = require('fs');


const crearArchivo2 = async (base = 5) => {

  // const base = 5;
  let salida = '';
  
  console.log('=================');
  console.log(`  Resta del ${base}  `);
  console.log('=================');
  
  
  for(let i = 1; i <= 100; i++){
    salida+= `${base} - ${i} = ${base-i}\n`;
  }
  
  console.log(salida);
  

  
  fs.writeFile(`Resta-${base}.txt`, salida, (err)=> {
    if (err) throw err;
      console.log(`Resta-${base}.txt creado`);
  });
}

//Exportamos el archivo
module.exports = {
  generarArchivo2: crearArchivo2
};