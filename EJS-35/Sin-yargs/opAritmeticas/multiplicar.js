//Estoy importando el paquete file system (fs)
const fs = require('fs');


const crearArchivo = async (base = 5) => {

  // const base = 5;
  let salida = '';
  
  console.log('=================');
  console.log(`  Tabla del ${base}  `);
  console.log('=================');
  
  
  for(let i = 1; i <= 100; i++){
    salida+= `${base} x ${i} = ${base*i}\n`;
  }
  
  console.log(salida);
  

  
  fs.writeFile(`tabla-${base}.txt`, salida, (err)=> {
    if (err) throw err;
      console.log(`tabla-${base}.txt creado`);
  });
}

//Exportamos el archivo
module.exports = {
  generarArchivo: crearArchivo
};