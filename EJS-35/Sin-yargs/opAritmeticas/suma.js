//Estoy importando el paquete file system (fs)
const fs = require('fs');


const crearArchivo3 = async (base = 5) => {

  // const base = 5;
  let salida = '';
  
  console.log('=================');
  console.log(`  Suma del ${base}  `);
  console.log('=================');
  
  
  for(let i = 1; i <= 100; i++){
    salida+= `${base} + ${i} = ${base+i}\n`;
  }
  
  console.log(salida);
  

  
  fs.writeFile(`Suma-${base}.txt`, salida, (err)=> {
    if (err) throw err;
      console.log(`Suma-${base}.txt creado`);
  });
}

//Exportamos el archivo
module.exports = {
  generarArchivo3: crearArchivo3
};